# Translating Anki's User Interface

Anki's translations are divided up into two parts:

- The `core` module contains text usable on all Anki platforms.
- The `desktop` module contains text only used in the computer version
  of Anki.
